
// console.log("Hello JSON!")

// [Section] JSON Objects
/*
    -JSON stands for JavaScript Object notation
    -JSON is also used in other programming languages hence the name JSON
    -Core JavaScript ahs a built in JSON object that containts method for parsing JSON objects and converting strings into JavaScript objects.
    -JSON is used for serializing different data types.
    Syntax:
    {
        "propertyA" : "valueB",
        "propertyB" : "valueB",
        "propertyC" : "valueB"
    }
*/

//JSON Object
// {
//     "city":"Quezon City",
//     "province":"Metro Manila",
//     "country":"Philippines"
// }

// // JSON Arrays:

// [{"city":"Quezon City", "province":"Metro Manila", "country":"Philippines"}, {"city":"Manila City", "province":"Metro Manila", "country":"Philippines"}]


// [section] JSON methods
    // The JSON object contains method for parsing and converting data into stringified JSON

    let batchesArray = [
            {
                batchName : "Batch X"
            },    
            {
                batchName : "Batch Y"
            }               
    ];
    console.log(batchesArray);
    // The stringify method is used to convert JavaScript objects into a string
    console.log("Result from stringify method: ");
    let stringBatchesArr = JSON.stringify(batchesArray);

    console.log(stringBatchesArr);
    
    let data = JSON.stringify(
        {
            name: 'John',
            address:{
                city: "Manila",
                country: "Philippines"
            }
        }
    )
    console.log(data);


// [section] Using stringify method with variables
// When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value to the variable.

// User details
// let firstName = prompt("What is your first name?");
// let lastName = prompt("What is your last name?");
// let age = prompt("How young are you?");
// let address = {
//     city: prompt("What city do you live in?"),
//     country: prompt("What country does your city address belong to?")
// }

// let otherData = JSON.stringify({
//     firstName: firstName,
//     lastName: lastName,
//     age: age,
//     address: address
// })

// let otherData2 = JSON.stringify({
//     firstName,
//     lastName,
//     age,
//     address
// })

// console.log(otherData);
// console.log(otherData2);


//SECTION Converting stringified JSON into JavaScript Objects

/*
        -Objects are common data types used in application because of the complex data structures that can be created out of them.

        -Information is commonly sent to applications in stringified JSON andd then converted back into objects
        -This happens both for sending information to a backend application and sending information back to a frontend application.
        
*/

let batchesJSON = `[{"batchName":"Batch X"},{"batchName":"Batch Y"}]`;
let parseBatchesJSON = JSON.parse(batchesJSON);
console.log(parseBatchesJSON);
console.log(parseBatchesJSON[0]);

let stringifiedObject = `{
    "name" :"John",
    "age" : "31",
    "address" : {
        "city": "Manila",
        "country" : "Philippines"
    }
}`

let parsestringifiedObject =JSON.parse(stringifiedObject);
console.log(parsestringifiedObject);
console.log(parsestringifiedObject.address.country);




